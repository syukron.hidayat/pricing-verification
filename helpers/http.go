package helpers

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"pricing_verification/models"
)

func Request(url, method string, payload interface{}) (*models.QEResponse, error) {
	b, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(b))
	if err != nil {
		log.Printf("error request : %+v", err)
		return nil, err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("error request do : %v", err)
		return nil, err
	}

	resBody, err := io.ReadAll(res.Body)
	if err != nil {
		log.Printf("error reading response : %+v", err)
		return nil, err
	}

	response := new(models.QEResponse)
	err = json.Unmarshal(resBody, &response)
	if err != nil {
		log.Printf("error unmarshal : %v", err)
		return nil, err
	}

	return response, nil
}
