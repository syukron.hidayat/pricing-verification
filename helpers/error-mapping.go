package helpers

import "strings"

func ErrorMapping(err string) string {
	if strings.Contains(err, "Unfortunately, we cannot insure this industry type") {
		return "Decline"
	}

	return err
}
