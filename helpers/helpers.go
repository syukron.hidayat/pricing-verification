package helpers

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"pricing_verification/models"
	"strconv"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
)

func Convert(data [][]string) []models.Pricing {
	var pricingList []models.Pricing

	for i, line := range data {
		if i > 0 {
			var rec models.Pricing
			rec.Warnings = ""
			for j, field := range line {
				colName := data[0][j]
				switch colName {
				case "Index":
					rec.Index = Int(field)
				case "POLNOR":
					rec.Polnor = field
				case "ClassCode":
					rec.ClassCode = ClassCodeFormat(field)
				case "fees_final":
					val := strings.ReplaceAll(field, ",", "")
					rec.Revenue = Int(val)
				case "si_final":
					val := strings.ReplaceAll(field, ",", "")
					rec.Coverage = Int(val)
				case "xs_final":
					rec.Deductible = Int(field)
				case "Commission":
					floatVal := Float(field)
					val := int(100 * floatVal)
					rec.Commission = fmt.Sprintf("%d", val)
				case "USA_Revenue":
					floatVal := Float(field)
					val := int(100 * floatVal)
					rec.UsaRevenue = int(val)
				case "UpGuard":
					rec.UpGuard = Int(field)
				case "Basic_BI":
					rec.Basic_BI = Clean(field)
				case "Standard_BI":
					rec.Standard_BI = Clean(field)
				case "Standard_ML":
					rec.Standard_ML = Clean(field)
				case "Standard_BP":
					rec.Standard_BP = Clean(field)
				case "Standard_PR":
					rec.Standard_PR = Clean(field)
				case "Advanced_BI":
					rec.Advanced_BI = Clean(field)
				case "Advanced_ML":
					rec.Advanced_ML = Clean(field)
				case "Advanced_BP":
					rec.Advanced_BP = Clean(field)
				case "Advanced_PR":
					rec.Advanced_PR = Clean(field)
				case "Advanced_Crime":
					rec.Advanced_Crime = Clean(field)
				case "Advanced_SocEng":
					rec.Advanced_SocEng = Clean(field)
				case "Output_Basic":
					rec.Output_Basic = field
				case "Output_Standard":
					rec.Output_Standard = field
				case "Output_Advanced":
					rec.Output_Advanced = field
				case "Warning1", "Warning2", "Warning3":
					if rec.Warnings != "" {
						rec.Warnings = rec.Warnings + "; " + field
					} else {
						rec.Warnings = field
					}
				}
			}
			pricingList = append(pricingList, rec)
		}
	}

	return pricingList
}

func ClassCodeFormat(code string) string {
	if strings.HasPrefix(code, "#") {
		return code
	}

	if len(code) == 3 {
		return fmt.Sprintf("#0%s", code)
	}

	return fmt.Sprintf("#%s", code)
}

func GetFileType(file *os.File) string {
	buf := make([]byte, 512)

	_, err := file.Read(buf)
	if err != nil {
		return ""
	}

	return http.DetectContentType(buf)
}

func ReadDataCSV(r io.Reader) []models.Pricing {
	log.Printf("reading file")
	fileReader := csv.NewReader(r)
	data, err := fileReader.ReadAll()
	if err != nil {
		log.Fatalf("Unable to read csv file : %v", err)
		return nil
	}

	return Convert(data)
}

func ReadDataExcel(r io.Reader) []models.Pricing {
	return []models.Pricing{}
}

func Int(val string) int {
	intval, err := strconv.Atoi(val)
	if err != nil {
		log.Printf("error converting %s to int", val)
		return 0
	}
	return intval
}

func Float(val string) float64 {
	floatval, err := strconv.ParseFloat(val, 64)
	if err != nil {
		log.Printf("error converting %s to float", val)
		return 0
	}
	return floatval
}

func Clean(val string) string {
	return strings.ReplaceAll(val, ",", "")
}

func Log(data interface{}) {
	byteData, _ := json.Marshal(data)
	var prettyJSON bytes.Buffer
	err := json.Indent(&prettyJSON, byteData, "", "   ")
	if err != nil {
		log.Fatalf("error log : %v", err)
	}

	log.Println(prettyJSON.String())
}

func LogTable(data []models.LogTable) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{
		"#",
		"Industry",
		"Actual Basic",
		"Basic Matched",
		"QI",
		"Actual Standard",
		"Standard Matched",
		"QI",
		"Actual Advanced",
		"Advanced Matched",
		"QI",
	})

	var rows []table.Row
	for _, d := range data {
		rows = append(rows, table.Row{
			d.Index,
			d.Industry,
			d.ActualBasic,
			d.ResultBasic,
			d.QIBasic,
			d.ActualStandard,
			d.ResultStandard,
			d.QIStandard,
			d.ActualAdvanced,
			d.ResultAdvanced,
			d.QIAdvanced,
		})
	}

	t.AppendRows(rows)
	t.AppendSeparator()
	t.Render()
}
