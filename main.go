package main

import (
	"encoding/csv"
	"log"
	"net/http"
	"os"
	"pricing_verification/helpers"
	"pricing_verification/models"
	"strings"
	"time"
)

var (
	VERBOSE_LOGS = false
)

var (
	BUSINESS_INTERRUPTION = "Business Interruption"
	MULTIMEDIA_LIABILITY  = "Multimedia Liability"
	BRAND_PROTECTION      = "Brand Protection"
	PERSONAL_REPUTATION   = "Personal Reputation Cover"
	CRIME                 = "Computer Crime Cover"
	SOCIAL_ENGINEERING    = "Social Engineering Fraud"

	BASIC    = "Basic"
	STANDARD = "Standard"
	ADVANCED = "Advanced"

	MINIMUM_UPGUARD_SCORE = 400
)

var (
	FILENAME           = "./files/20230622 Cyber Pricing Test Cases.csv"
	QUOTE_TYPE         = "cgu_1.0"
	GENERATE_QUOTE_URL = "http://localhost:25021/api/quotes/generate"

	TODAY_DATE = time.Now()
)

func initialPayload(data models.Pricing) models.GenerateQuoteRequest {
	return models.GenerateQuoteRequest{
		Domain:              "www.iag.com.au",
		QuoteType:           QUOTE_TYPE,
		Industry:            data.ClassCode,
		UpguardScore:        data.UpGuard,
		CommissionPercent:   data.Commission,
		USARevenuePercent:   data.UsaRevenue,
		Revenue:             data.Revenue,
		DatePolicyInception: TODAY_DATE,
		Inclusions:          []models.QuoteOptionInclusion{models.CyberCrime, models.SocialEngineering},
	}
}

func main() {
	raw := readCSV()

	pricingList := helpers.Convert(raw)

	var result []models.LogTable
	var matched []int
	var unmatched []int
	var basicMatched []int
	var standardMatched []int
	var advancedMatched []int
	for _, pricing := range pricingList {
		if pricing.Index > 0 {
			logs, quotes, err := initiateQuote(pricing)
			if err != nil {
				panic(err)
			}

			if logs == nil && quotes != nil {
				logs = recalculateQuote(pricing, quotes)
			}

			if logs == nil {
				continue
			}

			logs.Index = pricing.Index
			logs.Industry = pricing.ClassCode

			result = append(result, *logs)

			if logs.ResultBasic && logs.ResultStandard && logs.ResultAdvanced {
				matched = append(matched, logs.Index)
			} else {
				unmatched = append(unmatched, logs.Index)
			}

			if logs.ResultBasic {
				basicMatched = append(basicMatched, logs.Index)
			}
			if logs.ResultStandard {
				standardMatched = append(standardMatched, logs.Index)
			}
			if logs.ResultAdvanced {
				advancedMatched = append(advancedMatched, logs.Index)
			}
		}
	}

	// writeCSV(raw, result)

	// helpers.LogTable(result)
	helpers.Log(result)

	log.Printf("%d row(s) matched", len(matched))
	log.Printf("%d row(s) unmatched. %+v", len(unmatched), unmatched)
	log.Printf("Basic matched = %d/%d", len(basicMatched), len(result))
	log.Printf("Standard matched = %d/%d", len(standardMatched), len(result))
	log.Printf("Advanced matched = %d/%d", len(advancedMatched), len(result))
}

func initiateQuote(pricing models.Pricing) (*models.LogTable, []models.Quote, error) {
	payload := initialPayload(pricing)

	if payload.UpguardScore < MINIMUM_UPGUARD_SCORE {
		payload.UpguardScore = 500
	}

	res, err := generateQuote(payload)
	if err != nil {
		log.Printf("Error initiate quote : %v", err)
		return nil, nil, err
	}

	if VERBOSE_LOGS {
		log.Println("Initial Payload")
		helpers.Log(payload)

		log.Println("Initial Result")
		helpers.Log(res.Data)
	}

	if res.Status != 201 {
		var logs models.LogTable
		err := helpers.ErrorMapping(res.Data.Log)
		logs.ResultBasic = (pricing.Output_Basic == err)
		logs.ResultStandard = (pricing.Output_Standard == err)
		logs.ResultAdvanced = (pricing.Output_Advanced == err)

		logs.ActualBasic = err
		logs.ActualStandard = err
		logs.ActualAdvanced = err

		return &logs, nil, nil
	}

	return nil, res.Data.Quotes, nil
}

func recalculateQuote(pricing models.Pricing, quotes []models.Quote) *models.LogTable {
	var logs models.LogTable
	for _, quote := range quotes {
		expected := pricing.Output_Basic
		actual, errorLog, ip := "", "", ""
		var qi []models.QuotingIssue

		payload := initialPayload(pricing)

		sublimits := quote.Sublimits

		BI, BP, ML, PR, Crime, SocEng := "", "", "", "", "", ""

		BI = pricing.Basic_BI

		if quote.PlanName == STANDARD {
			expected = pricing.Output_Standard
			BI = pricing.Standard_BI
			BP = pricing.Standard_BP
			ML = pricing.Standard_ML
			PR = pricing.Standard_PR
			Crime = "100000"
		} else if quote.PlanName == ADVANCED {
			expected = pricing.Output_Advanced
			BI = pricing.Advanced_BI
			BP = pricing.Advanced_BP
			ML = pricing.Advanced_ML
			PR = pricing.Advanced_PR
			Crime = pricing.Advanced_Crime
			SocEng = pricing.Advanced_SocEng
		}

		for i, sl := range sublimits {
			for j, item := range sl.Items {
				switch item.Name {
				case BUSINESS_INTERRUPTION:
					sublimits[i].Items[j].Limit = BI
				case BRAND_PROTECTION:
					sublimits[i].Items[j].Limit = BP
				case MULTIMEDIA_LIABILITY:
					sublimits[i].Items[j].Limit = ML
				case PERSONAL_REPUTATION:
					sublimits[i].Items[j].Limit = PR
				case CRIME:
					sublimits[i].Items[j].Limit = Crime
				case SOCIAL_ENGINEERING:
					sublimits[i].Items[j].Limit = SocEng
				}
			}
		}

		payload.QuoteOptions = []models.QuoteOption{
			{
				PlanName:   quote.PlanName,
				Coverage:   pricing.Coverage,
				Deductible: pricing.Deductible,
				Sublimits:  sublimits,
			},
		}

		res, err := generateQuote(payload)
		if err != nil {
			log.Fatalf("error generate quote : %v", err)
			panic(err)
		}

		if VERBOSE_LOGS {
			log.Println("Final payload")
			helpers.Log(payload)

			log.Println("Final Result")
			helpers.Log(res)
		}

		if res.Status == 201 {
			qi = res.Data.Quotes[0].QuotingIssues
			ip = res.Data.Quotes[0].IndicativePremium
			actual = res.Data.Quotes[0].TechnicalPremium
			if actual == "999999999" {
				actual = ip
			}
		}

		qiStr := []string{}
		for _, q := range qi {
			qiStr = append(qiStr, string(q))
		}

		switch quote.PlanName {
		case BASIC:
			logs.ResultBasic = (expected == actual)
			logs.QIBasic = strings.Join(qiStr, ",")
			logs.IPBasic = ip
			if actual != "" {
				logs.ActualBasic = actual
			} else {
				logs.ActualBasic = errorLog
			}
		case STANDARD:
			logs.ResultStandard = (expected == actual)
			logs.QIStandard = strings.Join(qiStr, ",")
			logs.IPStandard = ip
			if actual != "" {
				logs.ActualStandard = actual
			} else {
				logs.ActualStandard = errorLog
			}
		case ADVANCED:
			logs.ResultAdvanced = (expected == actual)
			logs.QIAdvanced = strings.Join(qiStr, ",")
			logs.IPAdvanced = ip
			if actual != "" {
				logs.ActualAdvanced = actual
			} else {
				logs.ActualAdvanced = errorLog
			}
		}
	}

	return &logs
}

func generateQuote(payload models.GenerateQuoteRequest) (*models.QEResponse, error) {
	res, err := helpers.Request(GENERATE_QUOTE_URL, http.MethodPost, payload)

	return res, err
}

func readCSV() [][]string {
	log.Printf("opening file")
	f, err := os.Open(FILENAME)
	if err != nil {
		log.Fatalf("Unable to open file : %v", err)
		panic(err)
	}
	defer f.Close()

	log.Printf("reading file")
	fileReader := csv.NewReader(f)
	data, err := fileReader.ReadAll()
	if err != nil {
		log.Fatalf("Unable to read csv file : %v", err)
		panic(err)
	}

	return data
}

func writeCSV(lines [][]string, data []models.LogTable) bool {
	log.Println("Writing CSV")
	log.Printf("len : %d", len(data))
	columns := []string{
		"Actual_Basic",
		"QI_Basic",
		"IP_Basic",
		"Actual_Standard",
		"QI_Standard",
		"IP_Standard",
		"Actual_Advanced",
		"QI_Advanced",
		"IP_Advanced",
	}

	f, err := os.Create(FILENAME)
	if err != nil {
		panic(err)
	}

	w := csv.NewWriter(f)

	lines[0] = append(lines[0], columns...)
	if err := w.Write(lines[0]); err != nil {
		panic(err)
	}

	for i, d := range data {
		j := i + 1
		row := []string{
			d.ActualBasic,
			d.QIBasic,
			d.IPBasic,
			d.ActualStandard,
			d.QIStandard,
			d.IPStandard,
			d.ActualAdvanced,
			d.QIAdvanced,
			d.IPAdvanced,
		}
		lines[j] = append(lines[j], row...)
		if err := w.Write(lines[j]); err != nil {
			log.Fatalf("Error writing file: %v", err)
			panic(err)
		}
	}

	log.Println("Writing file has been completed!")

	w.Flush()
	f.Close()
	return true
}
