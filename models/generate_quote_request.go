package models

import "time"

type QuoteOptionInclusion string

const (
	MultimediaLiability     QuoteOptionInclusion = "MultimediaLiability"
	BrandProtection         QuoteOptionInclusion = "BrandProtection"
	CyberCrime              QuoteOptionInclusion = "CyberCrime"
	PersonalReputationCover QuoteOptionInclusion = "PersonalReputationCover"
	BusinessInterruption    QuoteOptionInclusion = "BusinessInterruption"
	SocialEngineering       QuoteOptionInclusion = "SocialEngineering"
	DataRecovery            QuoteOptionInclusion = "DataRecovery"
)

type GenerateQuoteRequest struct {
	QuoteType           string `json:"quoteType"`
	Id                  int    `json:"id,omitempty"`
	CompanyName         string `json:"companyName"`
	AddressStreetName   string `json:"addressStreetName"`
	AddressStreetNumber string `json:"addressStreetNumber"`
	AddressZipCode      string `json:"addressZipCode"`
	Domain              string `json:"domain"`
	Industry            string `json:"industry"`

	Revenue             int        `json:"revenue"`
	USARevenuePercent   int        `json:"usaRevenuePercent"`
	Employees           int        `json:"employees"`
	DatePolicyInception time.Time  `json:"datePolicyInception"`
	DatePolicyEnd       *time.Time `json:"datePolicyEnd"`
	PolicyLimit         int        `json:"policyLimit"`
	PriceAdjustment     float64    `json:"priceAdjustment"`
	RecordsPiiTotal     int        `json:"recordsPiiTotal"`
	UpguardScore        int        `json:"upguardScore,omitempty"`

	//Quote options are the result of the first generation and should not be populate when changing
	// the quote.
	QuoteOptions []QuoteOption `json:"quoteOptions"`

	//Property for overriding the base premium (only intended for underwriters)
	BasePremium string `json:"basePremium"`

	CommissionPercent string `json:"commissionPercent"`

	//Global inclusions to be used when generating new quotes. These are intended to be populated
	// based on RAQ responses and are used in conjunction with the default plan settings.
	Inclusions []QuoteOptionInclusion `json:"Inclusions,omitempty"`
}

type QuoteOption struct {
	Id                        int                    `json:"Id"`
	PlanName                  string                 `json:"PlanName"`
	Deductible                int                    `json:"Deductible,omitempty"`
	Coverage                  int                    `json:"Coverage,omitempty"`
	Sublimits                 []Sublimit             `json:"Sublimits,omitempty"`
	Inclusions                []QuoteOptionInclusion `json:"Inclusions,omitempty"`
	FirstPartyLimitPercentage string                 `json:"FirstPartyLimitPercentage,omitempty"`
}

type Sublimit struct {
	Name        string
	Description string
	Items       []SublimitItem
}

type SublimitItem struct {
	//Name of the sublimit
	Name string

	//Individual limit of the sublimit
	Limit string

	//The unit of the limit. This could be "$"" or "hours"
	Unit string

	// TODO: This has not been used; Related services need to be updated for this to be thru the user flows (referrals, for example)
	//Sort order for presentation - this shouldnt really be in quote engine but its the only place at the moment
	Order int

	//Individual deductible for the sublimit
	Deductible string
}
