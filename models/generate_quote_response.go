package models

type QuotingIssue string

type QuotesWithLog struct {
	Quotes []Quote
	Log    string
}

type Quote struct {
	Id         int
	Order      int
	PlanName   string
	Deductible int
	Coverage   string
	Currency   string

	//The annual premium calculated by the pricing algorithm. Customers (broker/customer) must not see the technical premium
	TechnicalPremium string

	//A premium to be used specified by the underwriter to be used instead of the technical premium. Where possible the technical
	// premium will still be calculated and provided to the underwriter for reference.
	OverwrittenPremium string

	//The minimum deductible allowed. Deductibles should be >= this value unless set by an underwriter.
	MinimumDeductible int

	//The premium prorated by policy length. It is calculated from the overwritten premum if provided, otherwise the technical premium.
	// Customers (broker/customer) must not see the prorated premium
	ProratedPremium string

	//If there are issues generating the quote which are out of bounds of the what is supposed to be provided to the algorithm but nethertheless
	// allow for a "valid" premium we store it as the indicitive premium. For these weird cases the underwriter would probably add additional price
	// to cater for additional risk. For instance if the excess is lower than the minimum we don't want to provide
	// a valid quote but we can provide an indication to the underwriter of what the price should be. Theoretically we should probably just return
	// the price in the technical premium field and handle this exceptional case upstream however for saftey we separate this so that its a low likelyhood
	// that a purchase is allowed at an unacceptable price.
	IndicativePremium string

	// details
	Commission Commission `json:"-"`
	Sublimits  []Sublimit
	CreatedBy  string
	UpdatedBy  string

	Inclusions []QuoteOptionInclusion `json:"Inclusions,omitempty"`

	//Issues found during quoting. If any quoting issues occured then tehcnical premium and prorated premium will be "999999999" and IndicativePremium will
	// have the calculated premium if possible. Refer to the discussion of IndicativePremium.
	QuotingIssues []QuotingIssue `json:"QuotingIssues,omitempty"`

	//These values are not used / computed by the quote engine but are in the official model.
	Premium      string
	Discount     string
	GrossPremium string
	NetPremium   string
}

type Fees struct {
	GST       float64
	StampDuty float64
}

// swagger:model Commission
type Commission struct {
	Commission     string `json:"Commission,omitempty"`
	CommissionRate string `json:"CommissionRate,omitempty"`
	CommissionGst  string `json:"CommissionGst,omitempty"`
}
