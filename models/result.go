package models

type QEResponse struct {
	Status  int           `json:"status"`
	Message string        `json:"message"`
	Data    QuotesWithLog `json:"data"`
}

type LogTable struct {
	Index          int
	Industry       string
	ActualBasic    string
	ResultBasic    bool
	QIBasic        string
	IPBasic        string
	ActualStandard string
	ResultStandard bool
	QIStandard     string
	IPStandard     string
	ActualAdvanced string
	ResultAdvanced bool
	QIAdvanced     string
	IPAdvanced     string
}

type QEResult struct {
	PlanName string
	Actual   string
	Error    string
}
