package models

type Pricing struct {
	Index           int
	Polnor          string
	ClassCode       string
	Revenue         int
	Coverage        int
	Deductible      int
	Commission      string
	UsaRevenue      int
	UpGuard         int
	Basic_BI        string
	Standard_BI     string
	Standard_ML     string
	Standard_BP     string
	Standard_PR     string
	Advanced_BI     string
	Advanced_ML     string
	Advanced_BP     string
	Advanced_PR     string
	Advanced_Crime  string
	Advanced_SocEng string
	Output_Basic    string
	Output_Standard string
	Output_Advanced string
	Warnings        string
}
